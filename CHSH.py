# authors: JOUFFROY Léopold, ENGEL Étienne
# coding: utf-8

import math
import numpy as np
from qiskit import *


def dec2bin(d,nb=0):
    """
    dec2bin(d,nb=0):
        conversion nombre entier positif ou nul -> chaîne binaire
        (si nb>0, complète à gauche par des zéros)
    """
    if d==0:
        b="0"
    else:
        b=""
        while d!=0:
            b="01"[d&1]+b
            d=d>>1
    return b.zfill(nb)

def E_parameter(list_counts):
    E = (list_counts[0]+list_counts[3]-list_counts[1]-list_counts[2])/sum(list_counts)
    return E

def probability(list_counts):
    return [list_counts[0]/sum(list_counts),list_counts[1]/sum(list_counts),list_counts[2]/sum(list_counts),list_counts[3]/sum(list_counts)]

def circuit_definition(circuit, string):   
    new_circuit = circuit.copy()
    if(string == 'ZW'):
            #first qbit
            new_circuit.measure(0,0)
            #second qbit
            new_circuit.ry(-np.pi/4, 1)
            new_circuit.measure(1,1)
    elif(string == 'ZV'):
            #first qbit
            new_circuit.measure(0,0)
            #second qbit
            new_circuit.ry(np.pi/4, 1)
            new_circuit.measure(1,1)
    elif(string == 'XW'):
            #first qbit
            new_circuit.h(0)
            new_circuit.measure(0,0)
            #second qbit
            new_circuit.ry(-np.pi/4, 1)
            new_circuit.measure(1,1)
    elif(string == 'XV'): 
            #first qbit
            new_circuit.h(0)
            new_circuit.measure(0,0)
            #second qbit
            new_circuit.ry(np.pi/4, 1)
            new_circuit.measure(1,1)
    return new_circuit

def circuit_execution(quantum_circuit, file_a='random_settings_a.txt', file_b='random_settings_b.txt', backend_def='qasm_simulator'):
    output_list = []
    config_list = ['ZW', 'ZV', 'XW', 'XV']
    with open(file_a, "r") as file:
        dataA = file.read()
    with open(file_b, "r") as file:
        dataB = file.read()
    for i in range(32768):
        selected_config = config_list[2*int(dataA[i]) + int(dataB[i])]       
        usable_circuit = circuit_definition(quantum_circuit, selected_config)
        simulator = Aer.get_backend(backend_def)
        result = execute(usable_circuit, backend=simulator, shots=1).result()
        result_dict = result.data(usable_circuit)['counts']
        for key in result_dict.keys():
            result_value = int(key[-1])
        result_value = dec2bin(result_value,2)
        output_list.append((selected_config, result_value))
    return output_list

def S_parameter(list_result):
    ZW_counts, ZV_counts, XW_counts, XV_counts = [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]
    for result in list_result:
        if result[0] == 'ZW':
            if result[1] == '00':
                ZW_counts[0] += 1
            elif result[1] == '01':
                ZW_counts[1] += 1
            elif result[1] == '10':
                ZW_counts[2] += 1
            elif result[1] == '11':
                ZW_counts[3] += 1
        elif result[0] == 'ZV':
            if result[1] == '00':
                ZV_counts[0] += 1
            elif result[1] == '01':
                ZV_counts[1] += 1
            elif result[1] == '10':
                ZV_counts[2] += 1
            elif result[1] == '11':
                ZV_counts[3] += 1
        elif result[0] == 'XW':
            if result[1] == '00':
                XW_counts[0] += 1
            elif result[1] == '01':
                XW_counts[1] += 1
            elif result[1] == '10':
                XW_counts[2] += 1
            elif result[1] == '11':
                XW_counts[3] += 1
        elif result[0] == 'XV':
            if result[1] == '00':
                XV_counts[0] += 1
            elif result[1] == '01':
                XV_counts[1] += 1
            elif result[1] == '10':
                XV_counts[2] += 1
            elif result[1] == '11':
                XV_counts[3] += 1
    
    E_ZW = E_parameter(ZW_counts)
    E_ZV = E_parameter(ZV_counts)
    E_XW = E_parameter(XW_counts)
    E_XV = E_parameter(XV_counts)
    
    S = E_ZW+E_ZV+E_XW-E_XV
    
    P_ZW = probability(ZW_counts)
    P_ZV = probability(ZV_counts)
    P_XW = probability(XW_counts)
    P_XV = probability(XV_counts)
    
    print("ZW",P_ZW,E_ZW)
    print("ZV",P_ZV,E_ZV)
    print("XW",P_XW,E_XW)
    print("XV",P_XV,E_XV)
    
    P_ZW_output = P_ZW
    P_ZW_output.append(E_ZW)
    P_ZV_output = P_ZV
    P_ZV_output.append(E_ZV)
    P_XW_output = P_XW
    P_XW_output.append(E_XW)
    P_XV_output = P_XV
    P_XV_output.append(E_XV)
    
    summary_result = [P_ZW_output, P_ZV_output, P_XW_output, P_XV_output]
    
    return S, summary_result


# Generate Bell state
circuit = QuantumCircuit(2, 2)
circuit.h(0)
circuit.cx(0, 1)
settingResult_list = circuit_execution(circuit)
S_param, table_result = S_parameter(settingResult_list)